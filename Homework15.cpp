﻿
#include <iostream>

void FindOddNumbers(int limit, bool isOdd)
{
    for (int i = 0; i < limit; i++)
    {
        if (isOdd)
        {
            if (i % 2 != 0)
            {
                std::cout << i;
            }
        }
        else if (!isOdd)
        {
            if (i % 2 == 0)
            {
                std::cout << i;
            }
        }

    }
}

int main()
{
    FindOddNumbers(10, true);//нечетные числа
    std::cout << std::endl;
    FindOddNumbers(10, false);//четные числа

    return 0;
}

